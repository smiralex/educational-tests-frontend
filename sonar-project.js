const sonarqubeScanner = require('sonarqube-scanner');

sonarqubeScanner(
    {
        serverUrl: "http://84.237.50.237:9000",
        token: "ac51f4c828e88b49159d8fe8d080a672b6c89d8b",
        options: {
            "sonar.sources": "./src",
            "sonar.exclusions": " **serviceWorker.js, **Test.js, **setupTests.js, **AxiosConfig.js",
            "sonar.tests": "./src/tests/",
            "sonar.javascript.lcov.reportPaths": "coverage/lcov.info",
            "sonar.test.inclusions": "**/src/tests/**/*.test.js",
            "sonar.testExecutionReportPaths": "testResults/sonar-report.xml",
        },
    },
    () => {},
);