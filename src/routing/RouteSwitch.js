import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import {Redirect, Switch} from "react-router-dom";
import AuthenticatedRoute from "./AuthenticatedRoute";
import UnauthenticatedRoute from "./UnauthenticatedRoute";
import Menu from "../components/Menu";
import Login from "../components/auth/Login";
import Profile from "../components/Profile";
import ChooseTheme from "../components/main/progress/ChooseTheme";
import ChooseSection from "../components/main/progress/ChooseSection";
import ChooseUnit from "../components/main/progress/ChooseUnit";
import ChooseStage from "../components/main/progress/ChooseStage";
import Theory from "../components/main/Theory";
import Test from "../components/main/Test";
import Registration from "../components/auth/Registration";
import PasswordRestore from "../components/auth/PasswordRestore";

/**
 * Глобальный свитч путей
 */
export default function RouteSwitch(props) {
    return (
        <Switch>
            <AuthenticatedRoute
                component={<Menu/>}
                path="/menu">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<Profile/>}
                path="/profile">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<ChooseTheme/>}
                path="/choose-theme">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<ChooseSection/>}
                path="/choose-section/:lastStageId">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<ChooseUnit/>}
                path="/choose-unit/:lastStageId">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<ChooseStage/>}
                path="/choose-stage/:lastStageId">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<Theory/>}
                path="/theory/:id">
            </AuthenticatedRoute>
            <AuthenticatedRoute
                component={<Test/>}
                path="/unit-:unitId/test-:testId">
            </AuthenticatedRoute>
            <UnauthenticatedRoute
                component={<Login/>}
                path="/login">
            </UnauthenticatedRoute>
            <UnauthenticatedRoute
                component={<Registration/>}
                path="/registration">
            </UnauthenticatedRoute>
            <UnauthenticatedRoute
                component={<PasswordRestore/>}
                path="/restore">
            </UnauthenticatedRoute>
            <Redirect from='/' to='/menu'/>
        </Switch>
    );
}

