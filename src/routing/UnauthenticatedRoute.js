import React from "react";
import {Redirect, Route} from "react-router-dom";
import {AuthContext} from "../context/AuthProvider";
import history from "./History";


/**
 * Путь роутера для компонентов, которые должны быть доступны толко незареганным пользователям
 */

export default function UnauthenticatedRoute({component, ...rest}) {
    const isAuthenticated = React.useContext(AuthContext).isAuthorized;
    return (
        !isAuthenticated
            ? <Route history={history} {...rest}>{component}</Route>
            : <Redirect to="/menu"/>
    )
}