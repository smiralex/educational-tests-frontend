import React from 'react';
import {shallow} from 'enzyme';
import {withContext} from 'shallow-with-context';
import PasswordRestore from "../../components/auth/PasswordRestore";

import {axiosNonApi} from "../../util/AxiosConfig";
import MockAdapter from "axios-mock-adapter";

let axiosMock;
describe('restore', () => {
    beforeEach(() => {
        axiosMock = new MockAdapter(axiosNonApi)
    });

    const context = {
        isAuthorized: false,
        login: jest.fn(),
        logout: jest.fn()
    }

    const RestoreWithContext = withContext(PasswordRestore)
    const wrapper = shallow(<RestoreWithContext/>, {context});

    it('works showModal', () => {
        wrapper.instance().state.onModalClose();
        expect(wrapper.state().showModal).toEqual(false)
    })

    it('restore renders', () => {
        expect(wrapper.find('ModalWindow')).toHaveLength(1)
    })

    it('works onResponseError', () => {
        const error = {response: {data: {message: 'test'}}}
        wrapper.instance().onRestoreError(error)
        expect(wrapper.state().showModal).toEqual(true)
    })


    it('works handleResponse', async () => {
        axiosMock.onPost().reply(200);
        wrapper.find('Form').simulate('submit', {
            preventDefault: jest.fn()
        });
        await new Promise(resolve => setTimeout(resolve, 0));
        expect(wrapper.state().showModal).toEqual(true)

    })

    describe('when typing into password input', () => {
        const email = 'sanka@gmail.com'

        beforeEach(() => {
            wrapper.find('Form').simulate('change', {
                target: {
                    value: email,
                    id: 'email'
                },
            })
        })

        it('updates email field in state', () => {
            expect(wrapper.state().email).toEqual(email)
        })
    })

    it('redirect works', () => {
        wrapper.instance().state.onModalClose()
        expect(wrapper.find('Redirect')).toHaveLength(1)
    })

});

