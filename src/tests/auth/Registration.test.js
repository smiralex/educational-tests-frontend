import React from 'react';
import {shallow} from 'enzyme';
import {withContext} from 'shallow-with-context';
import Registration from "../../components/auth/Registration";

describe('registration', () => {

    const context = {
        isAuthorized: false,
        login: jest.fn(),
        logout: jest.fn()
    }

    const RegWithContext = withContext(Registration)
    const wrapper = shallow(<RegWithContext/>, {context: context});

    it('workds showModal', () => {
        wrapper.instance().state.onModalClose();
        expect(wrapper.state().showModal).toEqual(false)
    })

    it('reg renders', () => {
        expect(wrapper.find('ModalWindow')).toHaveLength(1)
    })

    it('works onRegError', () => {
        const error = {response: {data: {message: 'test'}}}
        wrapper.instance().onRegistrationError(error)
        expect(wrapper.state().showModal).toEqual(true)
    })

    it('works onRegSuccess', () => {
        wrapper.instance().onRegistrationSuccess()
        expect(wrapper.state().showModal).toEqual(true)
    })

    describe('when typing into password input', () => {
        const email = 'sanka@gmail.com'

        beforeEach(() => {
            wrapper.find('Form').simulate('change', {
                target: {
                    value: email,
                    id: 'email'
                },
            })
        })

        it('updates email field in state', () => {
            expect(wrapper.state().email).toEqual(email)
        })
    })

    it('redirect registered works', () => {
        wrapper.instance().state.onModalClose();
        expect(wrapper.find('Redirect')).toHaveLength(1)
    })


});
