import React from 'react'
import {shallow} from 'enzyme'
import Login from '../../components/auth/Login'
import {withContext} from 'shallow-with-context';
import ModalWindow from "../../components/modals/ModalWindow";
import MockAdapter from "axios-mock-adapter";
import {axiosNonApi} from "../../util/AxiosConfig";

let axiosMock;
describe('Login', () => {
    beforeEach(() => {
        axiosMock = new MockAdapter(axiosNonApi)
    });

    const initialState = {
        login: '',
        password: '',
        modalMessage: '',
        modalTitle: '',
        showModal: false,
    }

    const context = {
        isAuthorized: false,
        login: jest.fn(),
        logout: jest.fn()
    }

    const LoginWithContext = withContext(Login, context)
    const login = shallow(<LoginWithContext/>, {context})

    it('initialize Login with initial state', () => {
        expect(login.state()).toEqual(initialState)
    })

    it('login renders', () => {
        expect(login.find('ModalWindow')).toHaveLength(1)
    })

    describe('Form handlers', () => { // обернули блок в describe
        describe('when typing into login input', () => {
            const name = 'sanka'

            beforeEach(() => {
                login.find('Form').simulate('change', {
                    target: {
                        value: name,
                        id: 'login'
                    },
                })
            })

            it('updates name field in state', () => {
                expect(login.state().login).toEqual(name)
            })
        })

        describe('when typing into password input', () => {
            const password = '11'

            beforeEach(() => {
                login.find('Form').simulate('change', {
                    target: {
                        value: password,
                        id: 'password'
                    },
                })
            })

            it('updates password field in state', () => {
                expect(login.state().password).toEqual(password)
            })
        })

        describe('when typing into password input', () => {
            const email = 'sanka@gmail.com'

            beforeEach(() => {
                login.find('Form').simulate('change', {
                    target: {
                        value: email,
                        id: 'email'
                    },
                })
            })

            it('updates email field in state', () => {
                expect(login.state().email).toEqual(email)
            })
        })

        afterAll(() => { // очистили state
            login.setState(initialState)
        })

        it('works handleAuth', () => {
            login.setContext(context)
            login.instance().handleAuthorize({data: {status: 200}})
            expect(context.login).toHaveBeenCalledTimes(1)
        })

        it('works onLoginError', () => {
            const error = {response: {data: {message: 'test'}}}
            login.instance().onLoginError(error)
            expect(login.state().showModal).toEqual(true)
        })

        it('works onModalClose', () => {
            login.instance().onModalClose()
            expect(login.state().showModal).toEqual(false)
        })
    })

    it('login success', () => {
        axiosMock.onPost().reply(200);
        login.find('Form').simulate('submit', {
            preventDefault: jest.fn()
        });
        expect(context.login).toHaveBeenCalledTimes(1)
    })

    it('redirect works', () => {
        context.isAuthorized = true
        login.setContext(context)
        expect(login.find('Redirect')).toHaveLength(1)
    })
})