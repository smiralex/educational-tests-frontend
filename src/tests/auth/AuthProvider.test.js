import React from 'react';
import {shallow} from 'enzyme';
import {AuthProvider} from "../../context/AuthProvider";

describe('Correct render', () => {
    let provider;
    const token = {token: 'jwt'};

    it('login', async () => {
        provider = shallow(<AuthProvider/>);
        provider.instance().login(token)
        expect(localStorage.getItem('jwt')).toEqual('jwt');
    });

    it('logout', async () => {
        provider = shallow(<AuthProvider/>);
        provider.instance().logout()
        expect(localStorage.getItem('jwt')).toEqual(null);
    });
});