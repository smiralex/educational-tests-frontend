import React from 'react';
import {shallow} from 'enzyme';
import {AXIOS} from "../../util/AxiosConfig";
import MockAdapter from "axios-mock-adapter";
import {Theory} from "../../components/main/Theory";

let axiosMock;
describe('Correct render', () => {
    beforeEach(() => {
        axiosMock = new MockAdapter(AXIOS)
    });
    const response = {id: 1, name: 'name', text: 'text'};

    it('renders correctly', async () => {
        let theory = shallow(<Theory match={{params: {id: 77}}}/>);
        axiosMock.onGet().reply(200, response);
        await new Promise(resolve => setTimeout(resolve, 0));
        expect(theory.instance().state).toEqual(response)
    });
});