import React from 'react';
import {shallow} from 'enzyme';
import {AXIOS} from "../../util/AxiosConfig";
import MockAdapter from "axios-mock-adapter";
import Test from "../../components/main/Test";

let axiosMock;
describe('Correct render', () => {
    let testMock;
    beforeAll(() => {
        axiosMock = new MockAdapter(AXIOS);
        testMock = shallow(<Test/>);
    });

    it('renders correctly', async () => {
        await new Promise(resolve => setTimeout(resolve, 0));
        // testMock.instance().setState({started: true});
        expect(testMock.find('Jumbotron')).toHaveLength(0)
    });

    // it('endTest', async () => {
    //     question.instance().setState({correctAnswerId: 777});
    //     axiosMock.onGet().reply(200, {
    //         questionText: 'testText',
    //         answerDtos: []
    //     });
    //     question.find('#nextQuestionButton').simulate('click', {
    //         preventDefault: jest.fn()
    //     });
    //     await new Promise(resolve => setTimeout(resolve, 0));
    //     expect(question.instance().state.questionText).toEqual('testText')
    // });
})
;