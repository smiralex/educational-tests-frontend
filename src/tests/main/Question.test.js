import React from 'react';
import {shallow} from 'enzyme';
import Question from "../../components/main/Question";
import {AXIOS} from "../../util/AxiosConfig";
import MockAdapter from "axios-mock-adapter";

let axiosMock;
describe('Correct render', () => {
    beforeEach(() => {
        axiosMock = new MockAdapter(AXIOS)
    });

    let question = shallow(<Question incrAnswers={() => console.log('incremented')}/>);
    it('renders correctly', () => {
        expect(question.find('Form')).toHaveLength(1)
    });

    it('shows next question', async () => {
        question.instance().setState({correctAnswerId: 777});
        axiosMock.onGet().reply(200, {
            questionText: 'testText',
            answerDtos: []
        });
        question.find('#nextQuestionButton').simulate('click', {
            preventDefault: jest.fn()
        });
        await new Promise(resolve => setTimeout(resolve, 0));
        expect(question.instance().state.questionText).toEqual('testText')
    });

    it('correct answer', async () => {
        let possibleAnswers = [
            {id: 777}, {id: 1}
        ];
        let correctAnswerId;
        question = shallow(<Question
            incrAnswers={() => console.log('incremented')}
            incrCorrect={() => correctAnswerId = 777}
        />);
        question.instance().setState({
            correctAnswerId: 777,
            selectedAnswerId: 1,
            possibleAnswers: possibleAnswers
        });
        axiosMock.onAny().reply(200, {
            id: 777,
            answerDtos: possibleAnswers
        });

        question.instance().answerCardClicked(777);
        await new Promise(resolve => setTimeout(resolve, 110));
        expect(correctAnswerId).toEqual(777)
    })


});