import React from 'react';
import {shallow} from 'enzyme';
import {AXIOS} from "../../util/AxiosConfig";
import MockAdapter from "axios-mock-adapter";
import {ChooseProgressPart} from "../../components/main/progress/ChooseProgressPart";

let axiosMock;
describe('Correct render', () => {
    const progressParts = [{id:1}];
    let choosePart;

    beforeEach(() => {
        axiosMock = new MockAdapter(AXIOS)
    });

    it('correct render', async () => {
        choosePart = shallow(<ChooseProgressPart match={{params:{lastStageId: 1}}}/>);
        choosePart.instance().setState({progressPartType: 'themes'});
        axiosMock.onGet().reply(200, {
            children: progressParts,
        });

        await new Promise(resolve => setTimeout(resolve, 110));
        expect(choosePart.find('Jumbotron')).toHaveLength(1);
        expect(choosePart.instance().state.progressParts).toEqual(progressParts)
    });

    it('render themes', async () => {
        choosePart = shallow(<ChooseProgressPart progressPartType="themes" match={{params:{lastStageId: 1}}}/>);
        choosePart.instance().setState({correctAnswerId: 77, });
        axiosMock.onGet().reply(200, progressParts);

        await new Promise(resolve => setTimeout(resolve, 0));
        expect(choosePart.instance().state.progressParts).toEqual(progressParts)
    });

    it('render stages', async () => {
        const progressParts = [{id:1, test: true}];

        choosePart = shallow(<ChooseProgressPart progressPartType="stages" match={{params:{lastStageId: 1}}}/>);
        choosePart.instance().setState({correctAnswerId: 77, });
        axiosMock.onGet().reply(200, {
            children: progressParts,
        });

        await new Promise(resolve => setTimeout(resolve, 0));
        expect(choosePart.find('ListGroup')).toHaveLength(1);
    });
})
;