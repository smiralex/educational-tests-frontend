import React from 'react';
import RouteSwitch from '../../routing/RouteSwitch';
import {shallow} from 'enzyme';

describe('routeSwitch', () => {

    it('renders correctly', () => {
        const wrapper = shallow(<RouteSwitch/>)
        expect(wrapper.find('Switch')).toHaveLength(1)
    })

})