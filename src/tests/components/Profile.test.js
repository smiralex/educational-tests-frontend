import React from 'react';
import Profile from '../../components/Profile';
import {shallow} from 'enzyme';

describe('profile', () => {

    const initialState = {
        email: '',
        fullName: '',
        levelName: '',
        levelNumber: 0,
        name: '',
        totalPoints: 0
    }

    const wrapper = shallow(<Profile/>)

    it('initialize Profile with initial state', () => {
        expect(wrapper.state()).toEqual(initialState)
    })

})