import React from 'react';
import Menu from '../../components/Menu';
import {shallow} from 'enzyme';
import {withContext} from 'shallow-with-context';

describe('menu', () => {
    const initialState = {
        exited: false
    }

    const context = {
        isAuthorized: false,
        login: jest.fn(),
        logout: jest.fn()
    }

    const MenuWithContext = withContext(Menu, context);
    const wrapper = shallow(<MenuWithContext/>, {context})

    it('initialize Menu with initial state', () => {
        expect(wrapper.state()).toEqual(initialState)
    })

    it('renders Nav', () => {
        expect(wrapper.find('Nav')).toHaveLength(1)
    })

    it('works оnSelect', () => {
        wrapper.setContext(context)
        wrapper.find('Nav').simulate('select', 'notexit')
        wrapper.find('Nav').simulate('select', 'exit')

        expect(wrapper.state().exited).toEqual(true)
        expect(context.logout).toHaveBeenCalledTimes(1)
    })

    it('not renders Nav', () => {
        expect(wrapper.find('Nav')).toHaveLength(0)
    })
})