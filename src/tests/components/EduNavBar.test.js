import React from 'react';
import EduNavBar from '../../components/EduNavBar';
import {shallow} from 'enzyme';

describe('eduNavBar', () => {

    it('renders correctly', () => {
        const eduNavBar = shallow(<EduNavBar/>);
        expect(eduNavBar.find('Navbar')).toHaveLength(1)
    })

})