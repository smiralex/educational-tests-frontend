import React from 'react';
import ModalWindow from '../../components/modals/ModalWindow';
import {shallow} from 'enzyme';
import {Modal} from "react-bootstrap";

describe('modal', () => {

    it('modal renders', () => {
        const modal = shallow(<ModalWindow/>)

        expect(modal.find(Modal)).toHaveLength(1)
    })

})