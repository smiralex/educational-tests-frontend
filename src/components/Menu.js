import React from 'react';
import {Jumbotron, Nav} from 'react-bootstrap';
import {Link, Redirect} from 'react-router-dom'
import {AuthContext} from "../context/AuthProvider";


export default class Menu extends React.Component {
    static contextType = AuthContext;

    constructor(props) {
        super(props);
        this.state = {
            exited: false
        }
    }

    onSelect = (eventKey) => {
        if (eventKey === "exit") {
            this.context.logout();
            this.setState({
                exited: true
            });
        }
    }

    render() {
        if (this.state.exited) {
            return <Redirect to='/login'/>;
        }
        return (
            <Jumbotron>
                <h1 align='center'>
                    Educational Tests
                </h1>
                <h3 align='center'>
                    Получай новые знания и проверяй свою эрудицию
                </h3>
                <br/>
                <Nav fill variant="tabs" onSelect={this.onSelect}>
                    <Nav.Item>
                        <Link to="/profile"
                              style={{color: 'blue', fontSize: '20px', fontWeight: 'bold'}}>
                            Профиль
                        </Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/choose-theme"
                              style={{color: 'green', fontSize: '20px', fontWeight: 'bold'}}>
                            Пройти тест
                        </Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="exit"
                                  style={{color: 'red', fontSize: '20px', fontWeight: 'bold'}}>
                            Выйти
                        </Nav.Link>
                    </Nav.Item>
                </Nav>
            </Jumbotron>
        );
    }
}
