import React from 'react';
import {Button, Col} from 'react-bootstrap';

export default function AnswerButton(props) {
    return (
        <Col key={props.answerId} md="auto">
            <Button
                style={{minHeight: '100px', width: '300px', fontSize: '150%'}}
                size="lg"
                variant={props.variant}
                onClick={props.onClick}>
                {props.children}
                <br/>
            </Button>
        </Col>
    );
}
