import React from 'react';
import {Jumbotron} from 'react-bootstrap';
import {AXIOS} from "../../util/AxiosConfig";
import {withRouter} from "react-router-dom";

export class Theory extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            name: '',
            text: ''
        }
    }

    componentDidMount() {
        AXIOS.get('/stages/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    name: response.data.name,
                    text: response.data.text
                })
            })
            .catch(error => console.log(error));
    }

    render() {
        return (
            <Jumbotron>
                <h1 align='center'>
                    {this.state.name}
                </h1>
                <h6 align='center' className='theoryText'>
                    {this.state.text}
                </h6>
            </Jumbotron>
        );
    }
}

export default withRouter(Theory);
