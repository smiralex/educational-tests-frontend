import ChooseProgressPart from "./ChooseProgressPart";
import React from "react";

export default function ChooseStage(props) {
    return (
        <ChooseProgressPart prevProgressPartType='units'
                            progressPartType='stages'
                            textH2='Вы можете посмотреть теорию или пройти тест по выбранной теме'
                            {...props}/>
    );
}