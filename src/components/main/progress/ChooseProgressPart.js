import React from 'react';
import {Jumbotron} from 'react-bootstrap';
import {AXIOS} from "../../../util/AxiosConfig";
import ListGroup from "react-bootstrap/ListGroup";
import {Link, withRouter} from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export class ChooseProgressPart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            progressParts: []
        }
    }

    componentDidMount() {
        if (this.props.progressPartType === 'themes') {
            AXIOS.get('/' + this.props.progressPartType)
                .then(response => {
                    this.setState({
                        progressParts: response.data
                    })
                })
                .catch(error => console.log(error));
        } else if (this.props.match.params.lastStageId !== undefined) {
            AXIOS.get('/' + this.props.prevProgressPartType + '/' + this.props.match.params.lastStageId)
                .then(response => {
                    this.setState({
                        progressParts: response.data.children
                    })
                })
                .catch(error => console.log(error));
        }
    }

    render() {
        let listItems;
        if (this.props.progressPartType === 'stages') {
            const theoryStages = this.state.progressParts
                .filter(progressPart => !progressPart.test)
                .map(progressPart =>
                    <ListGroup.Item as={Link} to={'/theory/' + progressPart.id} key={progressPart.id} action>
                        <span style={{color: 'blue'}}>
                            {progressPart.name}
                        </span>
                    </ListGroup.Item>
                );
            const testStages = this.state.progressParts
                .filter(progressPart => progressPart.test)
                .map(progressPart =>
                    <ListGroup.Item as={Link}
                                    to={'/unit-' + this.props.match.params.lastStageId + '/test-' + progressPart.id}
                                    key={progressPart.id} action>
                        <span style={{color: 'blue'}}>
                            {progressPart.name}
                        </span>
                    </ListGroup.Item>
                );
            listItems =
                <Container fluid>
                    <Row>
                        <Col align='center'>Теория</Col>
                        <Col align='center'>Тесты</Col>
                    </Row>
                    <Row>
                        <Col>{theoryStages}</Col>
                        <Col> {testStages}</Col>
                    </Row>
                </Container>
        } else {
            listItems = this.state.progressParts.map((progressPart) =>
                <ListGroup.Item as={Link} to={'/choose-' + this.props.nextProgressPartType + '/' + progressPart.id}
                                key={progressPart.id} action>
                    <span style={{color: 'blue'}}>
                        {progressPart.name}
                    </span>
                </ListGroup.Item>
            );
        }
        return (
            <div className="noBackgroundBlock">
                <Jumbotron>
                    <h1 align='center'>
                        {this.props.textH1}
                    </h1>
                    <h2 align='center'>
                        {this.props.textH2}
                    </h2>
                </Jumbotron>
                <ListGroup align='center' className="progressPartsList">
                    {listItems}
                </ListGroup>
            </div>
        );
    }
}

export default withRouter(ChooseProgressPart)