import React from 'react';
import ChooseProgressPart from "./ChooseProgressPart";

export default function ChooseTheme(props) {
    return (
        <ChooseProgressPart progressPartType='themes'
                            nextProgressPartType='section'
                            textH2='Выберите тему'
                            {...props}/>
    );
}
