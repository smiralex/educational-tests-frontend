import ChooseProgressPart from "./ChooseProgressPart";
import React from "react";

export default function ChooseUnit(props) {
    return (
        <ChooseProgressPart prevProgressPartType='sections'
                            progressPartType='units'
                            nextProgressPartType='stage'
                            textH2='Выберите блок '
                            {...props} />
    );
}