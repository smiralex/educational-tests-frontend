import ChooseProgressPart from "./ChooseProgressPart";
import React from "react";

export default function ChooseSection(props) {
    return (
        <ChooseProgressPart prevProgressPartType='themes'
                            progressPartType='sections'
                            nextProgressPartType='unit'
                            textH2='Выберите раздел'
                            {...props} />
    );
}
