import React from 'react';
import {Button, Col, Container, Form, Jumbotron, Row} from 'react-bootstrap';
import {AXIOS} from "../../util/AxiosConfig";
import AnswerButton from "./AnswerButton";

export default class Question extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            questionId: 0,
            questionText: '',
            possibleAnswers: [],
            correctAnswerId: null,
            selectedAnswerId: null
        }
    }

    componentDidMount() {
        this.nextQuestionClicked();
    }

    render() {
        const answerCards = this.state.possibleAnswers
            .map((answer, i) => this.formAnswerCard(answer, i));
        const nextQuestionButton = this.state.correctAnswerId ?
            <Button id="nextQuestionButton" onClick={this.nextQuestionClicked} style={{float: 'right'}} variant="outline-secondary">
                Следующий вопрос
            </Button> : null;
        return (
            <Form>
                <Jumbotron>
                    <h3 style={{textAlign: 'center'}}>
                        {this.state.questionText}
                    </h3>
                </Jumbotron>
                <Container fluid="md">
                    <Row className="justify-content-md-center">
                        {answerCards.slice(0, Math.floor(answerCards.length / 2))}
                    </Row>
                    <br/>
                    <br/>
                    <Row className="justify-content-md-center">
                        {answerCards.slice(Math.floor(answerCards.length / 2), answerCards.length)}
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            {nextQuestionButton}
                        </Col>
                    </Row>
                </Container>
            </Form>

        );
    }

    formAnswerCard(answer) {
        let buttonVariant = "info", onClick;
        if (answer.id === this.state.correctAnswerId) {
            buttonVariant = "success";
        } else if (answer.id === this.state.selectedAnswerId) {
            buttonVariant = "danger";
        } else if (!this.state.correctAnswerId) {
            onClick = e => this.answerCardClicked(answer.id, e);
        }

        return (
            <AnswerButton key={answer.id} variant={buttonVariant} onClick={onClick}>
                {answer.answerText}
            </AnswerButton>
        )
    }

    nextQuestionClicked = () => {
        AXIOS.get('/stages/test/' + this.props.testProgressId + '/next')
            .then(response => {
                if (response.data === "") {
                    this.props.endTest()
                } else {
                    this.setState({
                        questionId: response.data.id,
                        questionText: response.data.questionText,
                        possibleAnswers: response.data.answerDtos,
                        correctAnswerId: null,
                        selectedAnswerId: null
                    })
                }
            })
            .catch(error => console.log(error));
        this.props.incrAnswers();
    }

    answerCardClicked = (id) => {
        AXIOS.post('/stages/test/' + this.props.testProgressId + '/answer',
            {id: id, answerText: ''})
            .then(response => {
                console.log("IN ID: " + response.data.id)
                if (response.data.id === id) {
                    this.props.incrCorrect();
                }
                this.setState({
                    correctAnswerId: response.data.id,
                    selectedAnswerId: id
                })
            })
            .catch(error => console.log(error));
    }
}
