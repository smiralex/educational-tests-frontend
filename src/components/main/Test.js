import React from 'react';
import {Button, Jumbotron, Nav} from 'react-bootstrap';
import {AXIOS} from "../../util/AxiosConfig";
import Question from "./Question";
import {Redirect, withRouter} from "react-router-dom";
import ModalWindow from "../modals/ModalWindow";

class Test extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            started: false,
            testProgressId: 0,
            totalAnswers: 0,
            correctAnswers: 0,
            ended: false,
            moreTests: false,
            showResults: false,
            reachedPoints: 0,
            maxPoints: 0
        }
    }

    render() {
        let content;
        console.log(this.state)
        if (this.state.ended) {
            return <Redirect to="/menu"/>
        }
        if (this.state.moreTests) {
            return <Redirect to={'/choose-stage/' + this.props.match.params.unitId}/>
        } else if (!this.state.started) {
            content =
                <div>
                    <br/><br/><br/><br/><br/><br/><br/><br/>
                    <Button variant="success" size="lg" block onClick={this.onStartClicked}>
                        Начать тест!
                    </Button>
                </div>

        } else {
            content =
                <div>
                    <Nav className="justify-content-start">
                        <Nav.Item>
                            <Nav.Link>{'Вопрос № ' + this.state.totalAnswers}</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link>{'Правильных ответов: ' + this.state.correctAnswers}</Nav.Link>
                        </Nav.Item>
                    </Nav>
                    <Question testProgressId={this.state.testProgressId} endTest={this.endTest}
                              incrAnswers={this.incrAnswers} incrCorrect={this.incrCorrect}/>
                </div>
        }

        return (
            <Jumbotron>
                {content}
                <ModalWindow
                    show={this.state.showResults}
                    onModalClose={this.onCloseModal}
                    title={'Результаты'}
                    buttonMsg={'В меню'}
                    message={'Вау, вы набрали ' + this.state.reachedPoints
                    + ' баллов из ' + this.state.maxPoints + '!'}
                >
                    <Button variant="primary" onClick={this.moreTests}>
                        Пройти еще тесты!
                    </Button>
                </ModalWindow>
            </Jumbotron>
        )
    }

    onStartClicked = () => {
        AXIOS.post('/stages/test/' + this.props.match.params.testId + '/start')
            .then(response => {
                this.setState({
                    started: true,
                    testProgressId: response.data.userTestStageProgressId,
                })
            })
            .catch(error => console.log(error));
    }

    onCloseModal = () => {
        this.setState({ended: true});
    }

    moreTests = () => {
        this.setState({moreTests: true});
    }

    endTest = () => {
        AXIOS.get('/stages/test/' + this.state.testProgressId + '/results')
            .then(response => {
                this.setState({
                    showResults: true,
                    reachedPoints: response.data.currentPoints,
                    maxPoints: response.data.maxPoints,
                    totalAnswers: this.state.totalAnswers - 1
                })
            })
            .catch(error => console.log(error));
    }

    incrAnswers = () => {
        this.setState({
            totalAnswers: this.state.totalAnswers + 1,
        })
    }

    incrCorrect = () => {
        this.setState({
            correctAnswers: this.state.correctAnswers + 1
        })
    }
}

export default withRouter(Test)