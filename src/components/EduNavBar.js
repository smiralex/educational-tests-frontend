import React from 'react';
import {Navbar} from 'react-bootstrap';

import {Link} from "react-router-dom";
import {ArrowLeftCircle} from "react-bootstrap-icons";
import history from "../routing/History";

export default class EduNavBar extends React.Component {
    render() {
        return (
            <Navbar bg="dark" variant="dark" sticky="top"  expand="lg" className="stickyNav">
                {history.length &&
                <ArrowLeftCircle color="grey" size={30} onClick={history.goBack} className="backButton"/>}
                <Navbar.Brand><Link style={{textDecoration: 'none', color: 'white'}} to="/query">Educational
                    tests</Link></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>

            </Navbar>
        )
    }
}